swig -c++ -python -threads -I%TSF_INCLUDE_PATH% pyTSF.i

cl /c /std:c++14 /I%PYTHON_PATH%\include /I%TSF_INCLUDE_PATH% pyTSF_wrap.cxx

link /DLL /LIBPATH:%PYTHON_PATH%\libs /LIBPATH:%TSF_LIB_PATH% TeamSpaceFortress.lib TeamSpaceFortressSDL.lib pyTSF_wrap.obj

lib /OUT:_pyTSF.lib /LIBPATH:%PYTHON_PATH%\libs /LIBPATH:%TSF_LIB_PATH% TeamSpaceFortress.lib TeamSpaceFortressSDL.lib pyTSF_wrap.obj

del pyTSF_wrap.*