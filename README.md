# pyTSF for Windows

This repository contains the SWIG wrapper, build script, and OpenAI gym wrappers for TSF for Windows.

## Requirements

Building the Python wrapper for TSF assumes the following:

1.  Visual Studio has been installed.  Visual Studio can be downloaded from [here][visual studio].  This wrapper was tested using Visual Studio Community Edition 2019, but other versions should work just as well.

2.  Team Spacefortress (TSF) has been downloaded and built.  Specifically, `TeamSpaceFortress.lib` and `TeamSpaceFortressSDL.lib` need to have been build, and the include files available.  The Team Spacefortress repository is [here][tsf], which include build instructions for Windows.

3.  A 64-bit version of Python has been installed.  Python installers are available [here][python].  Python 3.9 may have some issues with SWIG, so using version 3.8 or earlier would be ideal.

4.  Swigwin needs to be installed to build the Python wrapper.  Swigwin 4.0.2 is available [here][swigwin].

[visual studio]: https://visualstudio.microsoft.com/vs/community/

[tsf]: https://gitlab.com/AART_STRONG/cycle1_seedling/tsf

[python]: https://www.python.org/downloads/windows/

[swigwin]: http://prdownloads.sourceforge.net/swig/swigwin-4.0.2.zip


## Building the Python Wrapper

Building the Python wrapper for TSF (pyTSF.py) involves building and compiling source files from the TSF swig interface.  For convenience, a build script is included in the repository under the `swig` directory.

In order to build, the `x64 Native Tools Command Prompt for VS 2019` command prompt must be used.  This is under the `Visual Studio 2019` folder in the Start Menu.  Using other command prompts may result in the wrapper being built, due to conflicts arising from code compiled for x86 and x64 architectures.  Additionally, this will likely need to be run as an administrator, in order to read the Python library and include files.

### Setting Environment Variables

In order to run the script, environment variables providing paths to the TSF library and include directories must be set, as well as an environment variable indicating the location of the Python directory.

#### TSF Library and Include Variables

The environment variables indicating the location of the TSF include and library paths must be named `TSF_INCLUDE_PATH` and `TSF_LIB_PATH`.  These can either be set in the Windows setting, or by executing the following commands in the command prompt

    set TSF_INCLUDE_PATH=<path_to_tsf_include_directory>
    set TSF_LIB_PATH=<path_to_tsf_library>

If the installation instructions were followed in the [TSF repository][tsf], and the `TSF_ROOT` environment variable is set, then the above environment variables can be set in terms of `TSF_ROOT`:

    set TSF_INCLUDE_PATH=%TSF_ROOT%\include
    set TSF_LIB_PATH=%TSF_ROOT%\lib

#### Python Path Variable

The `PYTHON_PATH` variable must be set to allow SWIG to be able to find the Python header files.  If Python was installed locally, then the location of the Python path can likely be set with the following command

    set PYTHON_PATH=c:\Users\<user_name>\AppData\Local\Programs\Python\Python<version>

Where `<user_name>` is the name of the user account that has Python installed, and `<version>` is the Python version number, (e.g., `Python39` for Python 3.9).  Note that `AppData` is typically a hidden directory in Windows, so may be difficult to find using File Explorer.

### Building

To build, run the `build.cmd` script in the `swig` folder

    cd swig
    build.cmd


